import React from 'react';
import './App.css';

function openBot() {
  window.open("https://bot.panterasbox.com/", "panterasbot", `toolbar=no, location=yes, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=yes, width=300, height=${window.screen.height}, left=${window.screen.width - 300}, top=0`, true);
}

function openSaxonberg() {
  window.open("https://saxonberg.panterasbox.com/", "_blank", "", true);
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Pantera's Sandbox<br/><br/>
        <img src="https://panterasbox.s3-us-west-1.amazonaws.com/rings.png" className="App-logo" alt="logo" /><br/>
        <button className="App-button" onClick={openBot}>Pantera's Bot</button><br/>
        <button className="App-button" onClick={openSaxonberg}>Saxonberg</button><br/>
      </header>
    </div>
  );
}

export default App;
