#!/bin/bash

CONTAINER=`docker ps -a -q --filter name=sandbox-web --format="{{.ID}}"`

if [ -z $CONTAINER ]
then echo No container running.
else docker rm $(docker stop $CONTAINER)
fi
