FROM node:14

COPY . /sandbox-web
WORKDIR /sandbox-web
RUN yarn install
RUN yarn build

EXPOSE 2080

CMD npm run server
